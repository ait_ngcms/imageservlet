package at.ait

import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import scala.collection.mutable.{ListBuffer, Map}
import java.io.PrintWriter
import java.net.{HttpURLConnection, URL}
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.Path
import java.util.logging.Logger

class ImageServlet extends HttpServlet {
  
  val pictures: ListBuffer[Picture] = ListBuffer()
  val teamScore: Map[String, Int] = Map()
  
  var lastUrl: String = "http://www.ait.ac.at/fileadmin/templates/tpl/images/ait_logo.jpg"
  var lastTeam: String = ""
  val log = Logger.getLogger("ImageServlet")

  /** For debug purposes ONLY 
  teamScore.put("Team Käpt'n Blaubär", 15)
  teamScore.put("Team Hein Blöd", 8)
  teamScore.put("Team Spongebob", 12)
  lastTeam = "Team Spongebob"
  **/
  
  override def doGet(req : HttpServletRequest, resp : HttpServletResponse) {
    try {
      val url = req.getParameter("url")
      if (url != null) {
        resp.addHeader("Access-Control-Allow-Origin", "*")
        val title = req.getParameter("title").replace("\"","\\\"")
        val team = req.getParameter("team")
        
        println("url: " + url + ", title: " + title + ", team: " + team)
        val found = pictures.find(url == _.source)
        if (found.isEmpty) {
          pictures += Picture(title, url, ListBuffer(team))
          increaseTeamScore(team)
          println("Picture is new. Score is now: " + teamScore(team))
          lastUrl = url
          lastTeam = "k&uuml;rzlich von Team " + team + " gefunden"
        }
        else if (found.get.teams.find(team == _).isEmpty) {
          found.get.teams += team
          increaseTeamScore(team)
          println("Picture was also found by another team. Score is now: " + teamScore(team))
        }
      } else if (req.getParameter("finish") != null) {
        for (picture <- pictures) {
          setFilename(picture)
          println("download picture " + picture.source + " to " + picture.image)
          if (download(picture)) {
            println("saving data as: " + picture.basename + ".json")
            val jsonWriter = new PrintWriter(picture.basename + ".json", "UTF-8")
            jsonWriter.write(picture.toString)
            jsonWriter.close()
          }
        }
        val statsWriter = new PrintWriter(getUniqueFilename("statistik", ".txt"), "UTF-8")
        statsWriter.println("Team\tBilder")
        teamScore.foreach(tuple => statsWriter.println(tuple._1 + "\t" + tuple._2))
        statsWriter.close()
      } else {
        println("showing picture: " + lastUrl)
        val sortedTeamList = teamScore.toList.sortBy(-_._2)
        resp.setContentType("text/html")
        val out = resp.getWriter
        
        out println """<!DOCTYPE html><html>
        <head>
          <meta http-equiv="refresh" content="15">
          <title>Scala Servlet</title>
          <!-- not the most subtle way - but one of those is always available, either in dev or prod mode -->
          <link rel="stylesheet" type="text/css" href="style.css">
          <link rel="stylesheet" type="text/css" href="imageservlet_2.11-0.0.1/style.css">
        </head>
        <body>
          <!-- h1>Team Statistik</h1 -->
          <div id="stats"><table>"""
        sortedTeamList.zipWithIndex.foreach { case ((teamName, score), idx) => 
          out.println("<tr>" +
                        "<td class=\"team-name\">" + teamName + "</td>" + 
                        "<td><div class=\"meter\"><div class=\"score\" style=\"width:" + (100 * score.toDouble / sortedTeamList(0)._2) + "%\">" + "</div></div></td>" +
                        "<td class=\"team-score\">" + score + " Bilder</td>" +
                      "</tr>") } 
        out println "</table></div><h2 id=\"img-caption\"><span>" + lastTeam + "</span></h2><img src=\"" + lastUrl + "\" />" +
        """</body></html>"""
      }
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }
  
  override def doPost(req : HttpServletRequest, resp : HttpServletResponse) {
    doGet(req, resp)
  }
  
  def increaseTeamScore(team: String) {
    val score = if (teamScore.contains(team)) teamScore(team) else 0
    teamScore += (team -> (score + 1))
  }
  
  def setFilename (picture: Picture) {
    var filename = picture.basename
    var i = 0
    var testpath: Path = null
    do {
      testpath = Paths.get(filename + ".json")
      picture.image = filename + picture.extension
      i += 1
      filename = picture.basename + i
    } while (Files.exists(testpath))
  }
  
  def getUniqueFilename (basename: String, ext: String) = {
    var filename = basename
    var i = 0
    var testpath: Path = null
    var result = ""
    do {
      testpath = Paths.get(filename + ext)
      result = filename + ext
      i += 1
      filename = basename + i
    } while (Files.exists(testpath))
    result
  }
  
  def download (picture: Picture) = {
    val path = Paths.get(picture.image)
    var success = false
    try {
      val serverAddress = new URL(picture.source)
      val connection = serverAddress.openConnection().asInstanceOf[HttpURLConnection]
      connection.setRequestMethod("GET")
      connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0")
      val stream = connection.getInputStream()
      try {
        Files.copy(stream, path)
        success = true
      } catch {
        case e: Exception => {
          e.printStackTrace()
          log.warning("cannot store picture: " + picture)
        }
      } finally {
        stream.close()
      }
      connection.disconnect()
    } catch {
      case e: Exception => {
        e.printStackTrace()
        log.warning("cannot download picture: " + picture)
      }
    }
    success
  }

}
