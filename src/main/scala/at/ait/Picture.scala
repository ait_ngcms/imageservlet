package at.ait

import scala.collection.mutable.ListBuffer

final case class Picture (title: String, source: String, teams: ListBuffer[String]) {
  override def toString = "{\"title\": \"" + title + "\"," +
      "\"image\": \"" + image + "\"," +
      "\"source\": \"" + source + "\"," +
      "\"collections\": [" + teams.mkString("\"", "\", \"", "\"") + "]}"
  var image =
    if (source.contains("?")) source.substring(source.lastIndexOf('/') + 1, source.indexOf('?'))
    else source.substring(source.lastIndexOf('/') + 1)
  def basename =
    if (image.contains(".")) image.substring(0, image.lastIndexOf('.'))
    else image
  def extension =
    if (image.contains(".")) image.substring(image.lastIndexOf('.'))
    else ""
}