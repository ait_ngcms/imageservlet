# Image Servlet

## Getting Started

1. The project uses SBT as a build system. Get SBT from [here](http://www.scala-sbt.org/release/tutorial/Setup.html).
  
2. Type `sbt package` to build a web application archive. The war-file is then located in `target/scala-2.11`.

3. You can deploy the war file with Tomcat (http://tomcat.apache.org/download-80.cgi). For auto-deploy copy the war file into the folder `{tomcat home}\webapps`.
Assuming a default configuration of Tomcat, the web service will be available at [http://localhost:8080/imageservlet_2.11-0.0.1](http://localhost:8080/imageservlet_2.11-0.0.1).

4. The bookmarklet generator can be found at [http://localhost:8080/imageservlet_2.11-0.0.1/bookmarklet_generator.htm](http://localhost:8080/imageservlet_2.11-0.0.1/bookmarklet_generator.htm).

5. Type `sbt eclipse` to generate an Eclipse project

## Known Issues

- Tomcat (and possibly other servlet containers too) seems to have a timeout for responding to an HTTP request. Therefore it is currently not possible to download more than about 50 images in the finalisation phase without proper adjustments.
- Teamnames with special characters can cause trouble (especially: # ? & = / ").