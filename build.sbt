organization := "at.ait"

name := "ImageServlet"

version := "0.0.1"

scalaVersion := "2.11.2"

libraryDependencies ++= Seq(
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.2.v20140723" % "container",
  "org.eclipse.jetty" % "jetty-plus"   % "9.2.2.v20140723" % "container"
)

seq(webSettings :_*)
